package com.example.graphcanvasm3.screens.legend

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.unit.dp
import com.example.graphcanvasm3.data.GraphData
import com.example.graphcanvasm3.utils.Tags.TAG_LEGEND

@Composable
fun Legend(
    modifier: Modifier = Modifier,
    chartData: List<GraphData>
) {
    Column(modifier = modifier.testTag(TAG_LEGEND)) {
        chartData.chunked(2).forEach { row ->
            Row {
                LegendItem(row[0])
                LegendItem(row[1])
            }
        }
    }
}

@Composable
fun LegendItem(data: GraphData) {
    Row(
        modifier = Modifier
            .padding(12.dp)
            .semantics(mergeDescendants = true) { },
        verticalAlignment = Alignment.CenterVertically
    ) {
        Box(
            modifier = Modifier
                .size(12.dp)
                .background(data.color)
        )
        Spacer(modifier = Modifier.width(8.dp))
        Text(text = data.label)
    }
}