package com.example.graphcanvasm3.screens.graphs

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.graphcanvasm3.viewmodel.GraphsViewModel

@ExperimentalLifecycleComposeApi
@Composable
fun Graphs(modifier: Modifier = Modifier) {
    val viewModel = viewModel<GraphsViewModel>()
    val state by viewModel.uiState.collectAsStateWithLifecycle()

    GraphsDashboard(
        modifier = modifier.fillMaxSize(),
        state = state,
        handleEvent = viewModel::handleEvent
    )
}