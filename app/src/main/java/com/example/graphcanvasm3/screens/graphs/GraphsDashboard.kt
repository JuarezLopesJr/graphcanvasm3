package com.example.graphcanvasm3.screens.graphs

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.graphcanvasm3.data.GraphState
import com.example.graphcanvasm3.data.GraphsEvent

@Composable
fun GraphsDashboard(
    modifier: Modifier = Modifier,
    state: GraphState,
    handleEvent: (GraphsEvent) -> Unit
) {

}