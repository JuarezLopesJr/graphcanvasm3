package com.example.graphcanvasm3.viewmodel

import androidx.lifecycle.ViewModel
import com.example.graphcanvasm3.data.GraphState
import com.example.graphcanvasm3.data.GraphsEvent
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class GraphsViewModel : ViewModel() {
    private val _uiState = MutableStateFlow(GraphState())
    val uiState = _uiState.asStateFlow()

    fun handleEvent(event: GraphsEvent) {
        when (event) {
            GraphsEvent.DismissPicker -> {
                _uiState.update {
                    it.copy(showChartPicker = true)
                }
            }
            GraphsEvent.ShowPicker -> {
                _uiState.update {
                    it.copy(showChartPicker = false)
                }
            }

            is GraphsEvent.SetCharType -> {
                _uiState.update {
                    it.copy(selectedGraphType = event.chart, showChartPicker = false)
                }
            }
        }
    }
}