package com.example.graphcanvasm3.data

sealed class GraphsEvent {
    object ShowPicker : GraphsEvent()

    object DismissPicker : GraphsEvent()

    class SetCharType(val chart: GraphType) : GraphsEvent()
}