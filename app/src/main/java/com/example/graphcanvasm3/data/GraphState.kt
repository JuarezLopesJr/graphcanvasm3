package com.example.graphcanvasm3.data

import com.example.graphcanvasm3.utils.GraphsDataFactory

data class GraphState(
    val selectedGraphType: GraphType = GraphType.LINE,
    val showChartPicker: Boolean = false,
    val chartData: List<GraphData> = GraphsDataFactory.makeChartData()
)