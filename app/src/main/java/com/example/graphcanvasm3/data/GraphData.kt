package com.example.graphcanvasm3.data

import androidx.compose.ui.graphics.Color

data class GraphData(
    val label: String,
    val value: Int,
    val color: Color
)