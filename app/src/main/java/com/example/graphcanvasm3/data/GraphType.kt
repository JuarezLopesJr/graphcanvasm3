package com.example.graphcanvasm3.data

import androidx.annotation.StringRes
import com.example.graphcanvasm3.R

enum class GraphType(@StringRes val type: Int) {
    PIE(R.string.graph_pie),
    LINE(R.string.graph_line),
    BAR(R.string.graph_bar),
    COLUMN(R.string.graph_column),
    DOUGHNUT(R.string.graph_doughnut),
    AREA(R.string.app_name)
}