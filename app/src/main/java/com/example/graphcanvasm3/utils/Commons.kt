package com.example.graphcanvasm3.utils

import android.graphics.Paint
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.DrawStyle
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.graphcanvasm3.data.GraphData
import com.example.graphcanvasm3.screens.legend.Legend
import com.example.graphcanvasm3.utils.Tags.TAG_CHART_DOUGHNUT
import com.example.graphcanvasm3.utils.Tags.TAG_CHART_PIE
import kotlin.math.cos
import kotlin.math.sin

@Composable
fun DoughnutChart(
    modifier: Modifier = Modifier,
    chartData: List<GraphData>
) {
    ArcChart(
        modifier = modifier.testTag(TAG_CHART_DOUGHNUT),
        chartData = chartData,
        useCenter = false,
        style = Stroke(150f, cap = StrokeCap.Butt),
        outerMargin = 70f
    )
}

@Composable
fun PieChart(
    modifier: Modifier = Modifier,
    chartData: List<GraphData>
) {
    ArcChart(
        modifier = modifier.testTag(TAG_CHART_PIE),
        chartData = chartData,
        useCenter = true,
        style = Fill
    )
}

@Composable
fun ArcChart(
    modifier: Modifier = Modifier,
    chartData: List<GraphData>,
    useCenter: Boolean,
    style: DrawStyle,
    outerMargin: Float = 0f
) {
    val transitionAnimation = configureAnimation(chartData)
    val radiusBorder = with(LocalDensity.current) { 24.dp.toPx() }
    val resources = LocalContext.current.resources
    val labelTextPaint = remember {
        labelTextPaint(resources).apply {
            textAlign = Paint.Align.CENTER
        }
    }.apply {
        alpha = ((255 / 100) * (transitionAnimation.value * 100)).toInt()
    }
    /* getting the sum of the entire data set */
    val sumOfDataSet = remember(chartData) { chartData.sumOf { it.value } }

    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceEvenly
    ) {
        Canvas(
            modifier = Modifier.size(240.dp)
        ) {
            /* keeping a track of the angle of the circle that has been drawn so far
               this means that i'll be drawing the circle segment by segment,
               building on the segments that were drawn before the current entry.
               the end of the previous segment is the start of the next */
            var currentSegment = 0f
            var currentStartAngle = 0f
            val radius = size.width / 2
            val totalAngle = 360

            chartData.forEach { chartData ->
                /* setting every angle: (data value divided by total sum of data) multiplied by 360
                  each segment size (piece of pie chart) = value of data
                   as a % of the total degrees */
                val segmentAngle = totalAngle * chartData.value / sumOfDataSet
                val angleToDraw =
                    totalAngle * (chartData.value * transitionAnimation.value) / sumOfDataSet
                drawArc(
                    color = chartData.color,
                    startAngle = currentStartAngle,
                    sweepAngle = angleToDraw,
                    useCenter = useCenter,
                    style = style
                )
                currentSegment += segmentAngle
                currentStartAngle += angleToDraw

                drawIntoCanvas {
                    val medianAngle = (currentSegment - (segmentAngle / 2)) * Math.PI / 180f
                    val radiusWithBorder = radius + radiusBorder
                    val drawAtX =
                        ((radiusWithBorder + outerMargin) * cos(medianAngle)).toFloat() + radius
                    val drawAtY =
                        ((radiusWithBorder + outerMargin) * sin(medianAngle)).toFloat() + radius
                    it.nativeCanvas.drawText(
                        chartData.value.toString(),
                        drawAtX,
                        drawAtY,
                        labelTextPaint
                    )
                }
            }
        }
        Spacer(modifier = Modifier.height(80.dp))
        Legend(chartData = chartData)
    }
}

@Preview
@Composable
fun DoughnutPreview() {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        DoughnutChart(chartData = GraphsDataFactory.makeChartData())
    }
}