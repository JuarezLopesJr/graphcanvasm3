package com.example.graphcanvasm3.utils

object Tags {
    const val TAG_CHART_TYPE_TOGGLE = "chart_type_toggle"
    const val TAG_LEGEND = "legend"
    const val TAG_CHART_BAR = "chart_bar"
    const val TAG_CHART_COLUMN = "chart_column"
    const val TAG_CHART_PIE = "chart_pie"
    const val TAG_CHART_DOUGHNUT = "chart_doughnut"
    const val TAG_CHART_AREA = "chart_area"
    const val TAG_CHART_LINE = "chart_line"
    const val TAG_SELECTED_CHART = "selected_chart"
    const val TAG_CHART_TYPES = "chart_types"
}